<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use App\Http\Controllers\IndexController;

// use App\Http\Controllers\IndexController;

Route::get('/', function () {
    return view('layout.master');
});

Route::get('/table_tab', function () {
    return view('ini_table.table');
});

Route::get('/table', function () {
    return view('ini_table.table');
});

Route::get('data-tables', function () {
    return view('ini_table.data-tables');
});
