<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function selamatDatang()
    {
        return view('halaman.selamat_datang');
    }

    public function master()
    {
        return view('layout.master');
    }
}
